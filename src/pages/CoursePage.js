/*import CourseCard from '../components/CourseCard'
import coursesData from '../mockData/coursesData'
*/
import UserView from '../components/UserView';
import AdminView from '../components/AdminView';
import { useContext, useEffect, useState } from 'react';
import UserContext from '../UserContext';


export default function CoursePage(){

	const [ allCourses, setAllCourses ] = useState([])

	const fetchData = () => {
		fetch('https://course-bruh.herokuapp.com/courses/all')
		.then(res => res.json())
		.then(data => {
			console.log(data)
			//storing all the data to our useState allCourses
			setAllCourses(data)
		})
	}

	useEffect(() => {
		fetchData()
	},[])


	const { user } = useContext(UserContext);
	/*//check if the mock data was captured.
		console.log(coursesData);
		console.log(coursesData[0]);//getting PHP laravel

		//for us to be able to display all the courses from the data  file we are going to use .map() method.

		const courses = coursesData.map(individualCourse => {
			return(
				<CourseCard key={individualCourse.id}courseProp = {individualCourse}/>
				)
		})*/
	return(
		<>
			<h1 className ="mt-3 text-center">Courses</h1>

			{(user.isAdmin === true)?

				<AdminView coursesData={allCourses} fetchData={fetchData} />

				:

				<UserView coursesData={allCourses} />

			} 

		</>

		)
}