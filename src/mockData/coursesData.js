const coursesData = [
	{
		id: "wdc001",
		name: "PHP - Laravel",
		description: "Lorem ipsum, dolor sit amet consectetur adipisicing, elit. Maxime earum inventore optio aliquid animi tenetur aut neque dolor iure harum quae provident quasi impedit adipisci dolores quas facilis, quis assumenda",
		price: 45000
	},
	{
		id: "wdc002",
		name: "Python _ Django",
		description: "Lorem ipsum, dolor sit amet consectetur adipisicing, elit. Maxime earum inventore optio aliquid animi tenetur aut neque dolor iure harum quae provident quasi impedit adipisci dolores quas facilis, quis assumenda",
		price: 50000
	},
	{
		id: "wdc003",
		name: "Java - Springboot",
		description: "Lorem ipsum, dolor sit amet consectetur adipisicing, elit. Maxime earum inventore optio aliquid animi tenetur aut neque dolor iure harum quae provident quasi impedit adipisci dolores quas facilis, quis assumenda",
		price: 55000
	}
]

export default coursesData;