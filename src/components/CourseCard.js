import { useState, useEffect } from 'react';
import { Card, Button , Row , Col } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default function CourseCard({courseProp}) {

	//deconstruct the course properties into their own variables
	const { _id, name, description, price } = courseProp;

	//use the state hook for this component to be able to store its value
	//states are used to keep track of information related to individual components.

	//syntax: 
		// const [currentValue(getter), updatedValue(setter)] = useState(initialGetterValue)

		// const [count, setCount] = useState(0);
		// const [seat, setSeat] = useState(30);

		//for the enable/disable of enroll button
		//const [isOpen, setIsOpen] = useState(true);

		/*useEffect(() => {
			if (seat === 0) {
				setIsOpen(false)
			}
		}, [seat])*/

		/*function enroll() {
			if(seat > 0){
				setCount(count+1);
				console.log('Enrollees: ' + count);
				setSeat(seat - 1)
				console.log('seats available: ' + seat);
			}
		}

		

*/	return(
		
		<Card className="my-3">
			<Card.Body>
				<Card.Title> {name}</Card.Title>
				<Card.Subtitle>Description</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>Php {price}</Card.Text>

					<Button variant="primary" as={ Link } to={`/courses/${_id}`}>Details</Button>
				
			</Card.Body>
		</Card>

		)
}



//Check if the CourseCard components is getting the correct prop types.
//Proptypes are used for validating information passed through a component and is as tool normally used to help developers ensure the correct information is passed from one component to another.

CourseCard.propTypes = {
	//Shape method is used to check if  a prop object has the smae specific shape of data type
	courseProp: PropTypes.shape({
		//define the properties and their expected data types
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})

}